![wela](https://www.dropbox.com/s/b0605a7f1nw7709/wela.png?dl=1)
---
# Welasensor Control Panel

Панель сбора данных с датчиков Welasensor, можно заводить и подключать любое 
количество токовых клещей, реле и датчиков, собирать данные на сервер на котором 
развернуто данное приложение. 

### Для установки необходимо: 

- Сервер >1Gb RAM, >5Gb HHD, Ubuntu/Debian
- Python 3, pip3

### Установка

// soon