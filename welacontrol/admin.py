from django.contrib import admin
from welacontrol.models import *

# Register your models here.
admin.site.register(Zone)
admin.site.register(Sensor)
admin.site.register(Data)
admin.site.register(Limits)
