from django.apps import AppConfig


class WelacontrolConfig(AppConfig):
    name = 'welacontrol'
    verbose_name = 'Welasensor Control'
