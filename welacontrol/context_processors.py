from welacontrol.models import Zone

def zones(request):
    return {'zones': Zone.objects.all()}
