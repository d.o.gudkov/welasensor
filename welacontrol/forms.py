# from django.shortcuts import render
from django import forms
from welacontrol.models import Zone, Limits

class LimitForm(forms.Form):
    sensor = forms.IntegerField()
    type = forms.CharField()
    max = forms.DecimalField()
    min = forms.DecimalField()

    class Meta:
        model = Limits

    def __init__(self, *args, **kwargs):
        super(LimitForm, self).__init__(*args, **kwargs)

class SensorForm(forms.Form):
    mac = forms.CharField()
    name = forms.CharField()
    zone = forms.ModelChoiceField(
        queryset=Zone.objects.all(),
        empty_label=None,
        widget=forms.Select(attrs={'class': 'form-control'})
    )

    def __init__(self, *args, **kwargs):
        super(SensorForm, self).__init__(*args, **kwargs)

class ZoneForm(forms.Form):
    name = forms.CharField()
    notes = forms.CharField()

    def __init__(self, *args, **kwargs):
        super(ZoneForm, self).__init__(*args, **kwargs)
