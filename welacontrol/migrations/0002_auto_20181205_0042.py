# Generated by Django 2.1.2 on 2018-12-04 21:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('welacontrol', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Zone',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=15, verbose_name='Имя')),
                ('notes', models.TextField(blank=True, verbose_name='Комментарии')),
            ],
        ),
        migrations.AlterField(
            model_name='sensor',
            name='name',
            field=models.CharField(blank=True, default='', max_length=15, verbose_name='Название'),
        ),
        migrations.AddField(
            model_name='sensor',
            name='zone',
            field=models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, to='welacontrol.Zone', verbose_name='Зона'),
        ),
    ]
