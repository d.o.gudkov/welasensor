from django.contrib.postgres.fields import ArrayField
from django.conf import settings
from django.db import models
from django.utils import timezone

# Create your models here.
class Zone(models.Model):
    name = models.CharField(max_length=15, default='', verbose_name='Имя', )
    notes = models.TextField(verbose_name='Комментарии', blank=True, )

    def __str__(self):
        return self.name

class Sensor(models.Model):
    zone = models.ForeignKey('Zone', on_delete=models.CASCADE, verbose_name='Зона', default='',)
    name = models.CharField(max_length=15, default='', verbose_name='Название', blank=True )
    mac = models.CharField(max_length=20, default='00-00-00-00-00', verbose_name='MAC адрес', )

    def is_online(self):
        last_data = Data.objects.filter(sensor=self).order_by('-date_add')[:1]
        if (len(last_data) and last_data[0].date_add > (timezone.now() - timezone.timedelta(minutes=10))):
            return True
        return False

    def data_counter(self):
        return len(Data.objects.filter(sensor=self))

    def last_activity(self):
        data = Data.objects.filter(sensor=self).order_by('-date_add')[:1]
        if ( len(data) ):
            return data[0].date_add
        return False

    def nice_mac(self):
        return self.mac.replace("-", " ")

    def get_limits(self):
        return Limits.objects.get(sensor=self)

class Data(models.Model):
    sensor = models.ForeignKey('Sensor', on_delete=models.CASCADE, verbose_name='Датчик', )
    date_add = models.DateTimeField(auto_now_add=True, )
    lux = models.PositiveIntegerField(default=1, verbose_name='Освещенность', )
    temperature = models.DecimalField(max_digits=6, decimal_places=2, verbose_name='Температура', )
    pressure = models.DecimalField(max_digits=6, decimal_places=2, verbose_name='Давление', )
    db = models.IntegerField(default=1, verbose_name='Шум', )
    humidity = models.DecimalField(max_digits=6, decimal_places=2, verbose_name='Влажность', )
    co2 = models.CharField(max_length=10, default='', verbose_name='CO2', )

    def __str__(self):
        return "#{id}. Данные от датчика {sensor}".format(id=self.id, sensor=self.sensor.id)

    @staticmethod
    def get_max(type, delta=False):
        if (delta):
            return Data.objects.filter(date_add__gt=timezone.now()-timezone.timedelta(days=delta)).aggregate(models.Max(type))
        return Data.objects.all().aggregate(models.Max(type))

    @staticmethod
    def get_min(type, delta=False):
        if (delta):
            return Data.objects.filter(date_add__gt=timezone.now()-timezone.timedelta(days=delta)).aggregate(models.Min(type))
        return Data.objects.all().aggregate(models.Min(type))

    @staticmethod
    def get_avg(type, delta=False):
        if (delta):
            return Data.objects.filter(date_add__gt=timezone.now()-timezone.timedelta(days=delta)).aggregate(models.Avg(type))
        return Data.objects.all().aggregate(models.Avg(type))

class Limits(models.Model):
    sensor = models.ForeignKey('Sensor', on_delete=models.CASCADE, verbose_name='Датчик', )
    lux = ArrayField(models.DecimalField(max_digits=6, decimal_places=2,), verbose_name='Лимиты освещения', size=2, blank=True, default=list,)
    temperature = ArrayField(models.DecimalField(max_digits=6, decimal_places=2,), verbose_name='Лимиты температуры', size=2, blank=True, default=list,)
    humidity = ArrayField(models.DecimalField(max_digits=6, decimal_places=2,), verbose_name='Лимиты влажности', size=2, blank=True, default=list,)
    co2 = ArrayField(models.DecimalField(max_digits=6, decimal_places=2,), verbose_name='Лимиты CO2', size=2, blank=True, default=list,)
