from django.urls import path
from welacontrol import views

urlpatterns = [
    #sidebar
    path('', views.home, name='home'),
    path('settings/', views.settings, name='settings'),
    #navbar
    path('tables/', views.tables, name='tables'),
    path('reports/', views.reports, name='reports'),
    path('docs/', views.docs, name='docs'),
    #app
    path('app/add/sensor/', views.sensor_add, name='sensor_add'),
    path('app/add/zone/', views.zone_add, name='zone_add'),
    path('app/zones/<int:zone_id>/', views.zones, name='zones'),
    path('app/post/', views.post_data, name='post_data'),
]
