from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.template.defaultfilters import slugify
# from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
# from django.shortcuts import get_object_or_404, render
from django.urls import reverse
# from django.views import generic
# from django.conf import settings
# from django.contrib.auth import login as auth_login, logout as auth_logout, authenticate
# from django.contrib.auth.models import User
# from django.contrib.postgres.search import SearchVector
# from django.core.mail import send_mail
# from django.utils.html import strip_tags
from welacontrol.forms import *
from welacontrol.models import *

# Create your views here.
def home(request):
    sensors = Sensor.objects.all()
    data = []
    for sensor in sensors:
        last_date = Data.objects.filter( sensor=sensor, date_add__gt=timezone.now()-timezone.timedelta(hours=24) ).order_by('-date_add')[:1]
        if(len(last_date)):
            data.append(last_date[0])

    form = LimitForm(request.POST or None)
    if form.is_valid():
        sensor_id = form.cleaned_data['sensor']
        type = form.cleaned_data['type']
        max = form.cleaned_data['max']
        min = form.cleaned_data['min']
        sensor = Sensor.objects.get(id=int(sensor_id))
        try:
            obj = Limits.objects.get(sensor=sensor)
            setattr(obj, type, [min, max])
            obj.save()
        except Limits.DoesNotExist:
            new_values = {'sensor': sensor, type: [min, max]}
            obj = Limits(**new_values)
            obj.save()
    status = {
        'pressure': {'current': Data.get_avg('pressure', 1), 'max': Data.get_max('pressure', 1), 'min': Data.get_min('pressure', 1), 'errors': None},
        'temperature': {'current': Data.get_avg('temperature', 1), 'max': Data.get_max('temperature', 1), 'min': Data.get_min('temperature', 1), 'errors': None},
        'humidity': {'current': Data.get_avg('humidity', 1), 'max': Data.get_max('humidity', 1), 'min': Data.get_min('humidity', 1), 'errors': None},
        'lux': {'current': Data.get_avg('lux', 1), 'max': Data.get_max('lux', 1), 'min': Data.get_min('lux', 1), 'errors': None},
    }
    return render(request, 'base.html', {'data': data, 'status': status})

@csrf_exempt
def post_data(request):
    mac = request.POST.get('mac')
    if len(Sensor.objects.filter(mac=mac)) > 0:
        data = Data(
            sensor = Sensor.objects.get(mac=mac),
            lux = request.POST.get('lux'),
            temperature = request.POST.get('temperature'),
            pressure = request.POST.get('pressure'),
            db = request.POST.get('db'),
            humidity = request.POST.get('humidity'),
            co2 = request.POST.get('co2'),
        )
        data.save()
        return JsonResponse({ 'response': 'ok' })
    return JsonResponse({ 'response': 'sensor not found' })

def sensor_add(request):
    form = SensorForm(request.POST or None)
    if form.is_valid():
        sensor = Sensor(
            zone = form.cleaned_data['zone'],
            mac = form.cleaned_data['mac'],
            name = form.cleaned_data['name']
        )
        sensor.save()
        return HttpResponseRedirect(reverse('home'))
    return render(request, 'forms/sensor_add.html', {'form': form})

def zone_add(request):
    form = ZoneForm(request.POST or None)
    if form.is_valid():
        name = form.cleaned_data['name']
        notes = form.cleaned_data['notes']
        zone = Zone(
            name = name,
            notes = notes,
        )
        zone.save()
        return HttpResponseRedirect(reverse('home'))
    return render(request, 'forms/zone_add.html', {'form': form})

def zones(request, zone_id):
    zone = Zone.objects.get(id=zone_id)
    sensors = Sensor.objects.filter(zone=zone)
    return render(request, 'zones.html', {'zone': zone, 'sensors': sensors})

def settings(request):
    return render(request, 'base.html', {})
def graphics(request):
    return render(request, 'base.html', {})
def tables(request):
    return render(request, 'base.html', {})
def docs(request):
    return render(request, 'base.html', {})
def reports(request):
    return render(request, 'base.html', {})

# Error pages
def page_not_found_view(request, exception=None):
    return render(request, 'errors/404.html', {})

def error_view(request):
    return render(request, 'errors/500.html', {})
